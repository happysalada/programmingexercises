// Sum all the prime numbers up to and including the provided number.
// A prime number is defined as a number greater than one and having only two divisors, one and itself. For example, 2 is a prime number because it's only divisible by one and two.
// The provided number may not be a prime.

// my solution is pretty horrible, I'm convinced there is a better solution
// OK i added a little simpler solution

function isPrime (num) {
  for (var i = 2; i < num; i++) {
    if (num % i === 0) {
      return false
    }
  }
  return true
}

function sumPrimes (num) {
  if (num < 0) {
    throw new Error('Only positive numbers supported.')
  }

  var sum = 0
  for (var i = 2; i <= num; i++) {
    if (isPrime(i)) {
      sum += i
    }
  }

  return sum
}

/**
 *  TESTS
 */

var expect = require('expect.js')

describe('sumPrimes', function () {
  it('returns 17 for sumPrimes(10)', function () {
    expect(sumPrimes(10)).to.be(17)
  })

  it('returns 73156 for sumPrimes(977)', function () {
    expect(sumPrimes(977)).to.be(73156)
  })
})
