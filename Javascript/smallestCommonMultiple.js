// Find the smallest common multiple of the provided parameters that can be evenly divided by both, as well as by all sequential numbers in the range between these parameters.
// The range will be an array of two numbers that will not necessarily be in numerical order.
// e.g. for 1 and 3 - find the smallest common multiple of both 1 and 3 that is evenly divisible by all numbers between 1 and 3.

// I hate this solution, but I couldn't figure out how to do better
// made a little better

// a common helper i always write when i am not using some logging library
var DEBUG = false
function log () {
  if (DEBUG) {
    console.log.apply(console, arguments)
  }
}

function isDivisibleByAllNumbers (number, testNumbers) {
  // probably the case that makes this return false is a higher number,
  // so start at the end of the array and work backwards. this is
  // also the best way to iterate an array when you dont care about order
  for (var i = testNumbers.length - 1; i >= 0; i--) {
    if (number % testNumbers[i] !== 0) {
      return false
    }
  }
  return true
}

function smallestCommonMultiple (a, b) {
  var A = Math.min(a, b)
  var B = Math.max(a, b)

  var sequentialRange = []
  for (var i = A; i <= B; i++) {
    sequentialRange.push(i)
  }
  log('Checking for common multiples in range:', sequentialRange)

  var maxMultiple = sequentialRange.reduce(function (sum, num) {
    return sum * num
  })
  log('Calculated maxMultiple=' + maxMultiple)

  for (var lowerMultiple = B; lowerMultiple < maxMultiple; lowerMultiple += B) {
    if (isDivisibleByAllNumbers(lowerMultiple, sequentialRange)) {
      log('Returning lowerMultiple: ' + lowerMultiple)
      return lowerMultiple
    }
  }

  log('Returning maxMultiple: ' + maxMultiple)
  return maxMultiple
}

/**
 *  TESTS
 */

// time taken for rest: before -> after changes
// smallestCommonMultiple(1, 5): 1.255ms -> 1.241ms
// smallestCommonMultiple(1, 13): 60.645ms -> 2.936ms
// smallestCommonMultiple(23, 18): 239.792ms -> 7.118ms

var expect = require('expect.js')

describe('smallestCommonMultiple', function () {
  it('returns 60 for smallestCommonMultiple(1, 5)', function () {
    // console.time('smallestCommonMultiple(1, 5)')
    expect(smallestCommonMultiple(1, 5)).to.be(60)
    // console.timeEnd('smallestCommonMultiple(1, 5)')
  })

  it('returns 360360 for smallestCommonMultiple(1, 13)', function () {
    // console.time('smallestCommonMultiple(1, 13)')
    expect(smallestCommonMultiple(1, 13)).to.be(360360)
    // console.timeEnd('smallestCommonMultiple(1, 13)')
  })

  it('returns 6056820 for smallestCommonMultiple(23, 18)', function () {
    // console.time('smallestCommonMultiple(23, 18)')
    expect(smallestCommonMultiple(23, 18)).to.be(6056820)
    // console.timeEnd('smallestCommonMultiple(23, 18)')
  })
})
