// Check if the predicate (second argument) is truthy on all elements of a collection (first argument).
// there has got to be a simpler way to do this one

function isTruthy (value) {
  if (typeof value === 'string') {
    return value === ''
  }
  return (
    value === false ||
    value === undefined ||
    value === null ||
    value === 0 ||
    isNaN(value)
  )
}

function truthCheck (collection, pre) {
  // Is everyone being true?
  return !collection.some(isTruthy)
}

/**
 *  TESTS
 */

var expect = require('expect.js')

describe('truthCheck', function () {
  it('returns true when all objects have a valid value for the given key', function () {
    var result = truthCheck([
      {'user': 'Tinky-Winky', 'sex': 'male'},
      {'user': 'Dipsy', 'sex': 'male'},
      {'user': 'Laa-Laa', 'sex': 'female'},
      {'user': 'Po', 'sex': 'female'}
    ], 'sex')
    expect(result).to.be.true
  })

  it('returns false when some objects have an invalid value for the given key', function () {
    var result = truthCheck([
      {'single': 'double'},
      {'single': NaN}
    ], 'single')
    expect(result).to.be.false
  })
})
