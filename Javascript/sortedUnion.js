// Write a function that takes two or more arrays and returns a new array of unique values in the order of the original provided arrays.
// In other words, all values present from all arrays should be included in their original order, but with no duplicates in the final array.
// The unique numbers should be sorted by their original order, but the final array should not be sorted in numerical order.

function uniteUnique (arr) {
  var argsArray = Array.prototype.slice.call(arguments)
  var concatenatedArray = argsArray.reduce(function (previousArray, currentArray) {
    return previousArray.concat(currentArray)
  })
  var filteredArray = concatenatedArray.filter(function (item, pos) {
    return (concatenatedArray.indexOf(item) === pos)
  })
  return filteredArray
}

/**
 *  TESTS
 */

// https://github.com/Automattic/expect.js
var expect = require('expect.js')

describe('uniteUnique', function () {
  it('should work with multiple 3 1D arrays', function () {
    var result = uniteUnique([1, 3, 2], [5, 2, 1, 4], [2, 1])
    expect(result).to.eql([1, 3, 2, 5, 4])
  })

  it('should work with multiple multi-dimension arrays', function () {
    var result = uniteUnique([1, 3, 2], [1, [5]], [2, [4]])
    expect(result).to.eql([1, 3, 2, [5], [4]])
  })

  it('should work not include duplicates', function () {
    var result = uniteUnique([1, 2, 3], [5, 2, 1])
    expect(result).to.eql([1, 2, 3, 5])
  })

  it('should work with multiple 4 1D arrays', function () {
    var result = uniteUnique([1, 2, 3], [5, 2, 1, 4], [2, 1], [6, 7, 8])
    expect(result).to.eql([1, 2, 3, 5, 4, 6, 7, 8])
  })
})
