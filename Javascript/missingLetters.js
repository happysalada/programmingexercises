function newFearNotLetter (str) {
  var letters = str.split('')

  var lastConsecutiveLetter = letters.find(function (letter, idx) {
    if (idx + 1 < letters.length) {
      var nextLetter = letters[idx + 1]
      var isConsecutive = (letter.charCodeAt(0) + 1) === nextLetter.charCodeAt(0)
      return !isConsecutive
    }
    return false
  })

  if (lastConsecutiveLetter) {
    return String.fromCharCode(lastConsecutiveLetter.charCodeAt(0) + 1)
  }
  return null
}

function originalFearNotLetter (str) {
  var strArray = str.split('')
  var letter = ''
  if (strArray.some(function (letr, id) {
    if (id + 1 < strArray.length) {
      var isConsecutive = (strArray[id].charCodeAt(0) + 1 === strArray[id + 1].charCodeAt(0))
      letter = letr
      return !isConsecutive
    }
  })) {
    return String.fromCharCode(letter.charCodeAt(0) + 1)
  }
  else {
    return undefined
  }
}

/**
 *  TESTS
 */

var expect = require('expect.js')

describe('originalFearNotLetter', function () {
  it('returns "d" when passed "abce"', function () {
    expect(originalFearNotLetter('abce')).to.be('d')
  })
})

describe('newFearNotLetter', function () {
  it('returns "d" when passed "abce"', function () {
    expect(newFearNotLetter('abce')).to.be('d')
  })
})
